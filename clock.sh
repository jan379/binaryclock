#!/usr/bin/env bash

minute=$(date +%-M)
hour=$(date +%-H)

/usr/local/bin/led 1 off
/usr/local/bin/led 2 off
/usr/local/bin/led 3 off
/usr/local/bin/led 4 off
/usr/local/bin/led 5 off
/usr/local/bin/led 6 off

display_time(){
led=6
time=$1
colour=$2
for binary in 32 16 8 4 2 1; do
	if [[ $time -ge $binary ]]; then
		/usr/local/bin/led $led $colour
		time=$(( time - binary ))
		((led--))
	elif [[ $time -lt $binary ]]; then
		/usr/local/bin/led $led off
		((led--))
	fi
done
}

for i in $(seq 3); do
 #echo $minute
 display_time $minute green
 sleep 10
 #echo $hour
 display_time $hour red
 sleep 10
done
