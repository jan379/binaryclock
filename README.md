# binaryClock

This is a small shell script that displays current time in binary format.
It is meant to display with only six leds available. Thus it willchange the colour
of the leds every ten seconds. With that we switch between hours and minutes.

The script can be triggered every minute.